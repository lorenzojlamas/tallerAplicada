A= [6 4;-1 2]
t = linspace(0,2*pi,2000);
figure;
hold on
axis equal

for i=1:2000
    plot(cos(t(i)),sin(t(i)),'.-r');
    y = A*[cos(t(i));sin(t(i))];
    plot(y(1),y(2),'.-b')
    pause(0.005);
end