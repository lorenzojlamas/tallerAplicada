n = 20;
I = eye(n);
A = I.*2;

%I = fila
%J = col
for i=1 :length(A)
    
    for j=1 :length(A)-1
        if(i == j)
            A(i,j+1) = 1;
            A(i+1,j) = 1;
        end
    end

end
[AutoVectorReal,autoValorReal]=eig(A);
autovalorReal=diag(autoValorReal);
[AutoValorRealmax,Indice]=max(abs(autovalorReal));
AutoVectorMax=AutoVectorReal(:,Indice)

k = 5;
x0 = (1:n);
x0 = x0/norm(x0);
x = x0';
difAutovalor(k)=0;
difAutovector(k)=0;
hold off
%lamda = 4;
lamdaSpace = linspace(3,4,20);

for j=1 :length(lamdaSpace)
    hold on
    lamda= lamdaSpace(j);
    for i=1 :k
        %resolver ( A-lamda*I)x = x
        x=(A-lamda*eye(n))\x;
        x=x/norm(x);
        lamda=x'*A*x;
        difAutovalor(i) = AutoValorRealmax-lamda;
        difAutovector(i) = x'*AutoVectorMax;
    end
    semilogy(abs(difAutovalor),'-o')
    %semilogy(abs(difAutovector),'-o')
end