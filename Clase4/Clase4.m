%%
%Cazador-Presa
tspan= [0,10];
p=1;
c=20;
a=30;
b=20;
delta= 50; 
gama=0.2;
n=0.2;

y0= [c,p];
F= @(x,y) [-a*y(1)+b*y(1)*y(2),delta *y(2)-gama*y(2)*y(2)-n*y(1)*y(2)]'
[x,y]= ode45 (F,tspan,y0');
plot(x,y)
plot (y(:,1),y(:,2)) 
%predadores en funcion de presas

%%
%Lotka Voltaire

tspan= [0,10];
c=linspace (1,10);
p=linspace (1,6);
a=20;
b=4;
delta= 10; 
gama=20;

F=@(x,y) x.^(-gama).* exp(x*delta) ./ (y.^a .* exp (-b*y))

[P,C]=meshgrid (p,c);
contour(P,C, F(P,C),1:10:500)
