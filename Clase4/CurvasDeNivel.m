%%
%Curvas de nivel

tspan= [0,10];
a=5;
b=4;
delta= 2; 
gama=5;
c=2;
p=6;
y0= [c,p];

F=@(x,y) [a*y(1)-b*y(1)*y(2),-gama*y(2)+delta*y(1)*y(2)]'


opciones.RelTol=1e-8;
opciones.AbsTol=1e-8;
[x,y]= ode45 (F,tspan,y0',opciones);

G=@(v,w) v.^(-gama).* exp(v*delta)./(w.^a.* exp(-b*w))

plot (x,G(y(:,1),y(:,2)))
