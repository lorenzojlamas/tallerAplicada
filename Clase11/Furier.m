%J = [1 2 5 8 5 2 1 0 0 0];


n = 100;
J = linspace(1,1000,n)
A = toeplitz(J);

%x = [1 2 3 4 5 6 7 1 2 3];

x = rand([1 n]);
b= A*x';

x1 = A\(b+randn(size(b)));


plot([x' x1],'-o')