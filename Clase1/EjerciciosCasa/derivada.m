%pkg load symbolic
%syms x

f = @(x) sin(x);

f1 = diff(f);

h = logspace(-20,0.1,1000);

fn  = matlabFunction(f)
f1n  = matlabFunction(f1)

xn = linspace(0.2,0.1,100);
errors = @(xn,h) (fn(xn+h)-fn(xn))/h - f1n(xn);
y1 = errors(xn,h);
y2 = fn(xn);

plot(xn,y1)

% h1 = 0.0001;
% x = -pi:h1:pi; 
% 
% f = sin(x);
% 
% f1 = diff(f)/h1;
% 
% h = logspace(-20,0.1,1000);
% 
% xn = linspace(0.2,0.1,100);
% 
% error = @(x,h) f(x)
% %errors = @(xn,h) (f(xn+h)-f(xn))/h - f1(xn);
% y1 = errors(x,h);
% y2 = fn(x);
% 
% plot(x,y1)