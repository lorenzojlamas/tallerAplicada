function [outputArg1,outputArg2] = test(inputArg1,inputArg2)
outputArg1 = inputArg1.*inputArg2;
outputArg2 = inputArg1.^inputArg2;
end

