N=1000; %Número de iteraciones.
t0=0; tN=100; h=(tN-t0)/N;  %Tiempo inicial, final y espacio entre iteraciones sucesivas.
a=0.4;b=0.37;c=0.3;d=0.05;  %Valor de las constantes del modelo
R(1)=3; F(1)=1;  %Condiciones iniciales

for i = 1:N
%Cálculo del próximo valor de R, F por el método de Euler.
R(i+1) = R(i) + h*R(i)*(a-c*F(i));
F(i+1) = F(i) + h*F(i)*(-b+d*R(i));
end;
T=t0:h:tN;

%Gráfica de la solución.
hold on
figure(1)
plot(T,R,'r')
plot(T,F,'g')
figure(2)
plot(R,F)
hold off