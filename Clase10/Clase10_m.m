load durer.mat
%imshow(X,map)

A = imread('./amy.jpeg');

n = length(A);
X = double(A);
R = X(:,:,1);
G = X(:,:,2);
B = X(:,:,3);

[UR,SR,VR] = svd(R);
[UG,SG,VG] = svd(G);
[UB,SB,VB] = svd(B);


for i = 1:n
    
    OUT(:,:,1)=UR(:,1:i)*SR(1:i,1:i)*VR(:,1:i)';
    OUT(:,:,2)=UG(:,1:i)*SG(1:i,1:i)*VG(:,1:i)';
    OUT(:,:,3)=UB(:,1:i)*SB(1:i,1:i)*VB(:,1:i)';
    
    imshow(uint8(OUT))
    title(int2str(i))
    pause
end