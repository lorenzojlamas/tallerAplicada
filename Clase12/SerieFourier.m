%% Serie de Fourier para sen(x)
N=100;
T=2;
w=2*pi/T;
f=@(x)sin(x);
a0=integral(f,0,T);
x=(linspace(0,T,1000))';
opciones.RelTol=1e-8;
opciones.AbsTol=1e-8;

for i=1:N %calculamos los coeficientes a(i) de la serie de fourier
    a(i)=(T/2)*(integral(@(x)f(x).*cos(x.*w*i),0,T,opciones));
    b(i)=(T/2)*(integral(@(x)f(x).*sin(x.*w*i),0,T,opciones));%calculamos los coeficientes b(i) de la serie de fourier
end

xx=linspace(-T,2*T,1000);
suma=a0/2;
for i=1:N
    suma=suma+a(i)*cos(i*w*xx)+b(i)*sin(i*w*xx); %suma de la serie de fourier
    sumaacum(i,:)=suma;
end

plot(x,f(x),xx,sumaacum')
loglog(1:N,abs(a))
%loglog(1:N,abs(b))
%% Serie de Fourier para una lineal
N=20;
T=2;
w=2*pi/T;
f=@(x)x;
a0=integral(f,0,T);
x=(linspace(0,T,1000))';
opciones.RelTol=1e-8;
opciones.AbsTol=1e-8;

for i=1:N %calculamos los coeficientes a(i) de la serie de fourier
    a(i)=(T/2)*(integral(@(x)f(x).*cos(x.*w*i),0,T,opciones));
    b(i)=(T/2)*(integral(@(x)f(x).*sin(x.*w*i),0,T,opciones));%calculamos los coeficientes b(i) de la serie de fourier
end

xx=linspace(-T,2*T,1000);
suma=a0/2;
for i=1:N
    suma=suma+a(i)*cos(i*w*xx)+b(i)*sin(i*w*xx); %suma de la serie de fourier
    sumaacum(i,:)=suma; %sumas parciales 
end

plot(x,f(x),xx,sumaacum') %me muestra como van mejorando las aproximaciones a medida que N crece
loglog (1:N,abs(b)) % como decrecen los coeficientes
loglog(1:N,abs(a)) %están muy cerca de cero con lo cual no me importan
