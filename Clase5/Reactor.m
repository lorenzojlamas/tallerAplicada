tspan= [0,10]
beta= 1;
gama= 10;
x0=10;
y0=0.1;
radio = 10;
angulos = linspace(1,60,200)
x1eQ = 8.23;
y1eQ = 9.18;

% for i =1 :length(angulos)
%     x1=x1eQ + radio*cos((2*pi*i)/max(angulos));
%     y1=y1eQ + radio*sin((2*pi*i)/max(angulos));
%     inicial= [x1,y1];
%     
%     F=@(x,y) [-y(1)*exp(-1/y(2))+gama*(x0-y(1));y(1)*exp(-1/y(2))+beta*(y0-y(2))];
%     opciones.RelTol=1e-8;
%     opciones.AbsTol=1e-8;
%     [x,y]= ode45(F,tspan,inicial',opciones);
%     plot(y(:,1),y(:,2))
%     hold on
% end
xmalla = linspace(-radio,radio,20);
ymalla = linspace(-radio,radio,20);
for j = 1 :length(xmalla)
   for i = 1 :length(ymalla)
    x1=x1eQ + xmalla(j);
    y1=y1eQ + ymalla(i);
    inicial= [x1,y1];
    F=@(x,y) [-y(1)*exp(-1/y(2))+gama*(x0-y(1));y(1)*exp(-1/y(2))+beta*(y0-y(2))];
    opciones.RelTol=1e-8;
    opciones.AbsTol=1e-8;
    [x,y]= ode45(F,tspan,inicial',opciones);
    plot(y(:,1),y(:,2))
    hold on
    end 
end

%axis equal



%plot(x,y)



% G=@(z) (gama*x0)./(gama+exp(-1./z))
% H=@(z) beta*(z-y0).*exp(1./z)
% 
% t=linspace(8,9,1000);
% 

% clf
% plot(t,G(t),'-')
% hold on
% plot(t,H(t),'-')