%Este programa calcula el ajuste algebraico de los puntos enlistados en M

function [xc,yc,r]=afg1(M) %donde M es de nx2

n=length (M(:,1));
L=[ones(n,1),M];

for i=1:n;
v(i)=((M(i,1))^2 + (M(i,2))^2);
endfor

w=L\(v');

C=w(1);
A=w(2);
B=w(3);

xc=A/2 ;
yc=B/2 ;
r=sqrt(C+(xc^2)+(yc^2)) ;
endfunction