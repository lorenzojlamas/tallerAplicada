% Realiza y grafica el ajuste geometrico a puntos de una circunferencia con ruido

v = linspace (0,2*pi,2000);
w1=rand(2000,1)-1/2;
w2=rand(2000,1)-1/2;
M = [50*cos(v')+2+w1,50*sin(v')+1+w2];  %tomamos el circulo de centro (2,1) y radio 4, pero funciona con otros valores
[x,y,r] = newton1 (M);

hold on

plot (r*cos(v)+x,r*sin(v)+y,'g');
plot (M(:,1),M(:,2),'.');