% Realiza y grafica el ajuste algebraico a puntos de una circunferencia con ruido

v = linspace (0,2*pi,100);
w1=rand(100,1)-1/2;
w2=rand(100,1)-1/2;
M = [4*cos(v')+2+w1,4*sin(v')+1+w2];  %tomamos el circulo de centro (2,1) y radio 4, pero funciona con otros valores
[x,y,r] = afg1 (M);

hold on

plot (r*cos(v)+x,r*sin(v)+y,'r');
plot (M(:,1),M(:,2),'.');
plot (4*cos(v)+2,4*sin(v)+1,'g');