%Este programa recibe una funcion f y estima su gradiente en el punto z

function [g] = grad(f,z) %f recibe tres variable y devuelve en R, por lo tanto z=(x,y,r)

h=0.0001;

a=z(1);
b=z(2);
c=z(3);

g1=(f([a+h,b,c])-f(z))/h;
g2=(f([a,b+h,c])-f(z))/h;
g3=(f([a,b,c+h])-f(z))/h;

g=[g1,g2,g3];

endfunction