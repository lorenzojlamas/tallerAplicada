%Esta funcion usa Newton-Raphson para calcular
%el ajuste geometrico de los puntos enlistados en M

function [xc,yc,r] = newton1(M)
%M es la matriz que da una lista de puntos con la forma (x,y)

n = length(M(:,1));

f= @ (x) (norm (norm (repmat([x(1),x(2)],n,1) - M,2,'rows') - x(3)))^2;
%Obs: Usamos los siguientes renglones (que ahora estan comentados)
%para cumplir la consigna del trabajo, pero visto que no aproxima bien,
%nos recomendaron reemplazarlo por
%v=[0,0,1];
v=[0,0,0];

for i=1:n;
v(1) = v(1) + M (i,1);
v(2) = v(2) + M (i,2);
endfor

v(1) = v(1) /n;
v(2) = v(2) /n;

for i=1:n;
v(3) = v(3) + sqrt ((v(1)-M(i,1))^2 + (v(2)-M(i,2))^2);
i=i+1;
endfor

v(3) = v(3) /n

i=1;
while i<100; 
v = v - (inv (hess (f,v)) * (grad (f,v))')';
i=i+1;
endwhile

xc=v(1);
yc=v(2);
r =v(3);
endfunction