% Realiza y grafica el ajuste geometrico a puntos de un arco de circunferencia con ruido

v = linspace (0,pi/4,1000);
w = linspace (0,2*pi,1000);

w1=rand(1000,1)-1/2;
w2=rand(1000,1)-1/2;
M = [4*cos(v')+2+w1,4*sin(v')+1+w2]; %tomamos el circulo de centro (2,1) y radio 4, pero funciona con otros valores

[x,y,r] = newton1 (M);

hold on

plot (r*cos(w)+x,r*sin(w)+y,'r');
plot (M(:,1),M(:,2),'.');
plot (4*cos(v)+2,4*sin(v)+1,'g');