data Polinomio = Mono Float Integer
				| Suma Polinomio Polinomio 
				| Producto Polinomio Polinomio 

evaluar :: Polinomio -> Float -> Float 
evaluar (Mono a b) x = a * (x^b)
evaluar (Suma p1 p2) x = evaluar p1 x + evaluar p2 x 
evaluar (Producto p1 p2) x = (evaluar p1 x) * (evaluar p2 x)

coeficientes :: Polinomio -> [Float]
coeficientes (Mono a 0) = [a]
coeficientes (Mono a b) = 0 ++ coeficientes (Mono a (b-1))
coeficientes (Suma p q) = sumarListas (coeficientes p) (coeficientes q)
coeficientes (Producto p q) = productoListas (coeficiente p) (coeficiente q)

sumarListas :: [Float] -> [Float] -> [Float]
sumarListas [] (y:ys) = (y:ys)
sumarListas (x:xs) [] = (x:xs)
sumarListas (x:xs) (y:ys) = (x+y) : (sumarListas xs ys)

productoListas [] _ = [] 
productoListas (x:xs) (y:ys) = sumarListas xys xsys
where 
		xys =productoCtePorLista x ys 
		xsys = 0 : (productoCtePorLista xs ys)

productoCtePorLista x ys = map (*x) ys 
