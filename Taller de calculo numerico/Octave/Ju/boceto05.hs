prodInt :: [Float] -> [Float] ->Float
prodInt a b | head a * head b + prodInt (tail a) (tail b)
