pertenece :: Integer -> [Integer] -> Bool
pertenece n l 	| l== [] = False  
		| n == head l = True 
		| pertenece n (tail l) = True
		| otherwise = False 

hayRepetidos :: [Integer] -> Bool
hayRepetidos l  | l == [] = False
		| not (pertenece (head l) (tail l)) = hayRepetidos (tail l) 
		| otherwise = True 

menores :: Integer -> [Integer] -> [Integer]
menores n l 	| l == [] = []
		| n > (head l) = head l : (menores n (tail l))
		| otherwise = (menores n (tail l))

quitar :: Integer -> [Integer] -> [Integer]
quitar n l 	| length l == 0 = []
		| n == (head l) = (tail l)
		| otherwise= (head l) : (quitar n (tail l) )

maximo :: [Integer] -> Integer 
maximo l | length l == 1 = head l  
	 | head l >  head (tail l) = maximo (head l : ( tail (tail l)))
	 | otherwise = maximo (tail l )






