data Direccion = Norte | Sur | Este | Oeste 
type Tortuga = (Pos, Direccion)
type Pos = (Integer, Integer)

arranca :: Tortuga 
arranca = ( (0,0), Norte)

girarDerecha :: Tortuga -> Tortuga 
girarDerecha (a, Norte) = (a, Este)
girarDerecha (a, Este) = (a, Sur)
girarDerecha (a, Sur) = (a, Oeste)
girarDerecha (a, Oeste) = (a, Norte)

avanzar :: Tortuga -> Integer -> Tortuga
avanzar ((x,y), Norte) z = ((x, y +z), Norte)
avanzar ((x,y), Este) z = ((x+z, y), Este)
avanzar ((x,y), Sur) z = ((x, y -z), Sur)
avanzar ((x,y), Oeste) z = ((x-z, y), Oeste)

data Figura = Rectangulo Float Float Float Float  
			| Circulo Float Float Float deriving (Show)

c1:: Figura 
c1 = Circulo 0 0 1

r1 :: Float -> Figura  
r1 x = Rectangulo 0 0 (sqrt x) (sqrt x) 


data ProgAritmetica = Vacio  | CongruentesA Integer Integer
instance Show ProgAritmetica where 
		show Vacio = "{}"
		show (CongruentesA x y) = "{a en z | a = " ++ show x ++ "(mod"++ show y ++ ")}"
instance Eq ProgAritmetica where 
	Vacio == Vacio = True
	Vacio == _ = False
	_ == Vacio = False
	CongruentesA a b == CongruentesA x y = incluido (CongruentesA a b) (CongruentesA x y) && incluido (CongruentesA x y) (CongruentesA a b) 

esMultiplo :: Integer -> Integer -> Bool 
esMultiplo a b = (mod a b) == 0

pertenece :: Integer -> ProgAritmetica -> Bool
pertenece _ Vacio = False 
pertenece c (CongruentesA a b) = (mod c b) == (mod a b)

incluido :: ProgAritmetica -> ProgAritmetica -> Bool
incluido (CongruentesA a b) (CongruentesA c d) = (esMultiplo b d) && (pertenece a  (CongruentesA c d))

iguales :: ProgAritmetica -> ProgAritmetica -> Bool 
iguales (CongruentesA a b) (CongruentesA c d) = (mod a b ) == (mod c d)
