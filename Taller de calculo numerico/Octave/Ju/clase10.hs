data Arbol = Hoja Integer | Ramificacion Arbol Integer Arbol deriving (Show)

esHoja :: Arbol -> Bool
esHoja (Hoja _) = True
esHoja (Ramificacion _ _ _ ) = False 

sumaNodos :: Arbol -> Integer
sumaNodos (Hoja x) = x
sumaNodos (Ramificacion izq x der) = x + sumaNodos izq +sumaNodos der 

altura :: Arbol -> Integer 
altura  (Hoja _ )= 1
altura (Ramificacion izq x der) = x + (max (altura izq) (altura der))

pertenece :: Integer -> Arbol -> Bool
pertenece n (Hoja x) = x == n
pertenece n (Ramificacion izq x der) | n == x = True
									 | otherwise = (pertenece n izq) || (pertenece n der )


data Dir = Der | Izq
busqueda :: [Dir] -> Arbol -> Integer
busqueda [] (Hoja x) = x 
busqueda [] (Ramificacion izq x der) = x
busqueda [] (Ramificacion izq x der) = x
busqueda (Der:xs) (Ramificacion izq x der) = busqueda xs Der
busqueda (Izq:xs) (Ramificacion izq x der) = busqueda xs Izq

espejar :: Arbol -> Arbol
espejar (Hoja a) = Hoja a 
espejar (Ramificacion izq a der) = (Ramificacion (espejar der) a (espejar izq))

esHoja :: Arbol -> Bool 
esHoja a 	| (altura a) == 1 = True
			| otherwise = False 

maximo :: Ord a => Arbol a -> a 
maximo (Hoja a) = a
maximo (Ramificacion izq a der) = (max izq (max iz a)) 