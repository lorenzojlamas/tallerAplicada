--distancia :: (Float, Float) -> (Float,Float) -> Float
--distancia p1 p2 = sqrt(((fst p1)-(fst p2))^2 + ((snd p1)-(snd p2))^2)

notienedivisoreshasta :: Integer -> Integer -> Bool 
notienedivisoreshasta m n 	| (mod 2 n ) == 0 = False
				| (mod m n) == 0 = False
				| (mod m n ) == 1 = True 
				| otherwise = notienedivisoreshasta (m-1) n

notienedivisoreshasta' :: Integer -> Integer -> Bool 
notienedivisoreshasta' m n	| (mod 2 n ) == 0 = False
				| otherwise = ((mod m n) == 0) && notienedivisoreshasta (m-1) n

enBase :: Integer -> Integer -> [Integer] 
enBase a b 	|  a <b = [a] 
		| otherwise = (mod a b ) : ( enBase (div a b ) b)

deBase :: Integer -> [Integer] -> Integer 
deBase a l 	| length l == 1 = 1
		| otherwise = (head l)^((length l)-1) + (deBase a (tail l))

capiPara :: [Integer] -> [Integer]
capiPara l | l == reverse l = l
	   | otherwise = capiPara (enBase 10 (deBase 10 l + deBase 10 (reverse l) ) )



