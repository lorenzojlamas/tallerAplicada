esPositivo x = x>0 

yLogico True True = True 
yLogico True False = False
yLogico False False = False 
yLogico False True = False

yLogico2 True True = True
yLogico2 x y  = False

funcion x y z | y<10 = x
	      | y>=10 = x + z 

nand True True = False 
nand x y = True

esPitagorica a b c = sqrt (a^2+b^2)==c

doble x = x+x

crearPar :: a -> b->(a,b)
crearPar x y = (x,y)

invertir :: (a,b) -> (b,a) 
invertir (x,y) = (y,x)

distancia :: (Float, Float) -> (Float,Float) -> Float
distancia p1 p2 = sqrt(((fst p1)-(fst p2))^2 + ((snd p1)-(snd p2))^2)

raices :: Float -> Float -> Float -> (Float, Float)
raices  a b c = ( (-b + sqrt (b^2-4 * a *c))/ (2*a) , (-b - sqrt (b^2 - 4*a*c))/(2 *a))

