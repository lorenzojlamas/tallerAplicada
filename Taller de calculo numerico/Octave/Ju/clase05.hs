suma:: [Integer]->Integer
suma l 	|l==[]=0
		|otherwise = (head l) + (suma (tail l))


reves :: [Integer] -> [Integer]
reves l | l == [] = []
	| otherwise = reves (tail l) ++ [head l]

capicua :: [Integer] -> Bool
capicua l | length l ==1 = True
	|length l ==0 = True
	|otherwise = ((head l == last l) && capicua(init (tail l )))
	

capicua2 l  | l == reverse l = True
		|otherwise = False 

prodInt :: [Float] -> [Float] ->Float
prodInt a b | a == [] = 0
	| b == [] = 0
	|otherwise = (head a) * (head b) + (prodInt (tail a) (tail b))

notienedivisoreshasta :: Integer -> Integer -> Bool 
notienedivisoreshasta m n 	| (mod 2 n ) == 0 = False 
				|(mod m n) == 0 = False
				| notienedivisoreshsata (m-1) n == False 
				| otherwise = True 
