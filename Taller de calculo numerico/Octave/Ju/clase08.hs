longitud :: [a] -> Integer 
longitud [] = 0
longitud (x:[]) = 1
longitud (x:y:[])= 2
longitud (x:y:z: []) =3
longitud (_:_:_:xs) = 8

--iniciales :: [Char] -> [Char] -> [Char]
--iniciales nombre apellido = [n ,n] 
--	where  (n:_) = nombre 
--	(a:_) = apellido

data Dia = Lunes | Martes | Miércoles |Jueves |Viernes |Sábado |Domingo deriving Eq
esFinde :: Dia -> Bool
esFinde Lunes= False
esFinde Martes= False
esFinde Miércoles= False
esFinde Jueves= False
esFinde Viernes= False
esFinde Sábado= True
esFinde Domingo= True

soloAlgebra :: [Dia] -> [Dia]
soloAlgebra [] = []
soloAlgebra (x:xs) | x == Martes = x : (soloAlgebra xs)
--soloAlgebra x == Viernes = x: (soloAlgebra xs) 
--soloAlgebra xs

type Conjunto = [Integer]
union :: Conjunto -> Conjunto -> Conjunto
union (x:xs) (y:ys) = (x:xs) ++(y:ys)

interseccion :: Conjunto -> Conjunto -> Conjunto
interseccion (x:xs) []	= []
interseccion  [] (y:ys)	= []
interseccion [] [] = []
interseccion (x:xs) (y:ys)| x ==y = x:(interseccion xs ys)
							| x /= y = interseccion (x:xs) ys

incluido :: Conjunto -> Conjunto -> Bool
incluido (x:xs) (y:ys) = interseccion (x:xs) (y:ys) == (x:xs)

igual :: Conjunto -> Conjunto -> Bool
igual (x:xs) (y:ys) = (incluido (x:xs) (y:ys)) && (incluido (y:ys) (x:xs))

igual2 :: Conjunto -> Conjunto -> Bool
igual2 (x:xs) (y:ys) = interseccion (x:xs) (y:ys) == (x:xs)