fib :: Integer -> Integer
fib n | n == 0 = 1
      | n == 1 = 1
      | n>1 = fib(n-1) + fib(n - 2)


par:: Integer -> Bool
par x | x == 0 = True
      | x == 1 = False
      | x > 0 = par(x-2)
      | x < 0 = par (x+2)

sumaImpares :: Integer -> Integer
sumaImpares n | n == 1 = 1
              | n > 0 = (n*2-1) + sumaImpares (n -1)

mult :: Integer -> Bool
mult n | n == 3 = True
       | n == 2 = False
       | n == 1 = False
       | n > 3 = mult(n - 3) 
       | n <= 0 = mult(n + 3)

doblefact :: Integer -> Integer
doblefact n | n == 0 = 1
            | n /= 0 = n * doblefact (n - 2)

comb :: Integer -> Integer -> Integer
comb n m | n < 0 = 0
         | n < m = 0
         | n == m = 1
         | n > m = ((comb (n-1) m) + (comb (n-1) (m-1)))

recursivaPares :: Integer-> Integer  
recursivaPares x | x==0 = 0
		 | x/=0 = recursivaPares (x-1)


