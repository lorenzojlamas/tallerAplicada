%Este programa recibe una funcion f y estima su matriz hessiana en el punto z

function [H]= hess(f,z) %f recibe un vector de R^3 y devuelve en R, por lo tanto z=(x,y,r)

h=0.0001;

H = zeros (3,3);
M = eye (3); %Es la matriz identidad
for i=1:3;
  for j=1:3;
   H(i,j) = (f(z+h*(M(j,:)+M(i,:))) - f(z+h*M(j,:)) - f(z+h*M(i,:)) + f(z)) / h^2;
endfor
endfor
endfunction